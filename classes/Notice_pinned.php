<?php

if (!defined('GNUSOCIAL')) { exit(1); }

/**
 * Table Definition for message_info
 */

class Notice_pinned extends Managed_DataObject
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'notice_pinned';                         // table name
    public $id;                              // int(4)  primary_key not_null
    public $profile_id;                      // int(4)  unique_key   not null
    public $notice_id;	                     // int(4)   not_null

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE

    public static function schemaDef()
    {
        return array(
            'fields' => array(
                'id' => array('type' => 'serial', 'not null' => true, 'description' => 'unique identifier'),
                'profile_id' => array('type' => 'int', 'not null' => true, 'description' => 'universally unique identifier'),
                'notice_id' => array('type' => 'int', 'not null' => true, 'description' => 'mark 1 if read')
                
            ),
            'primary key' => array('id'),

            'foreign keys' => array(
                'profile_id_profile_fkey' => array('profile', array('profile_id' => 'id')),
		'notice_id_notice_fkey' => array('notice', array('notice_id' => 'id'))
            ),
            'indexes' => array(
                // @fixme these are really terrible indexes, since you can only sort on one of them at a time.
                // looks like we really need a (to_profile, created) for inbox and a (from_profile, created) for outbox
                'profile_id_idx' => array('profile_id'),
                
            ),
        );
    }


}
?>