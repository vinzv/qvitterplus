var pinnedReplace = false;
$("body").ready(function(){checkLocalStorage = checkLocalStorageFixed;});

var DropDown = new CustomEvent(
	"DropDown", 
	{
		bubbles: true,
		cancelable: true
	}
);



$(document).ready(function(){

	//insertPinned();
	
	document.addEventListener("languageReady" , setUpDropDown);
	addProfileCardToDOM = addProfileCardToDOMFixed;
});



function searchForProfile(){
	if($('div:not(".hover-card") .profile-card:not(".pinned-post-cont")').length > 0 && ($('div:not(".hover-card") .profile-card:not(".pinned-post-cont")').find('.pinned-post-cont').length == 0) || pinnedReplace){
		
		//var old = qpGetItem( 'pinned' , $('div:not(".hover-card") .profile-card:not(".pinned-post-cont")').find('.profile-header-inner').attr('data-user-id'));
		//var old = qpGetItem( 'pinned' , 1);	
		var old = localStorageObjectCache_GET('qppinned' , $('div:not(".hover-card") .profile-card:not(".pinned-post-cont")').find('.profile-header-inner').attr('data-user-id'));
			
		if(old != null && !pinnedReplace && old != false){
			pinnedGet(JSON.parse(old));
			pinnedReplace = true;
			searchForProfile();
		} else {
		    insertPinned();
		}
	}
	
	
}

function insertPinned(){
	var card = $('div:not(".hover-card") .profile-card:not(".pinned-post-cont")');
	var id = $(card).find('.profile-header-inner').attr('data-user-id');
	
	
	getFromAPI("qvitterplus/pinned/get.json?user_id=" + id , insertPinnedPost);
}

function insertPinnedPost(data){
	if(data != null && data.notice_id != undefined){

		getFromAPI("statuses/show/" + data.notice_id + ".json" , pinnedGet);

	}
}

function pinnedGet(data){
	var card = $('div:not(".hover-card") .profile-card:not(".pinned-post-cont")');

	if(data.error == undefined || data.error != "No such notice."){
		//qpSetItem( 'pinned' , $(card).find('.profile-header-inner').attr('data-user-id') ,  JSON.stringify(data));
		localStorageObjectCache_STORE('qppinned' , $(card).find('.profile-header-inner').attr('data-user-id') ,  JSON.stringify(data));
	
	if($(card).find('.pinned-post-cont').length == 0){
		$(card).find('.clearfix').before(pinnedDom(data));
	} else if(pinnedReplace){
		$(card).find(".pinned-post-cont").remove();
		$(card).find('.clearfix').before(pinnedDom(data));
		pinnedReplace = false;
	}
	} else if($(card).find('.pinned-post-cont').length != 0){
		$(card).remove(".pinned-post-cont");
	}
	
	
}

function pinnedDom(data){
		var html = buildQueetHtml(data, data.id, "")
		var divHtml = "<div class='profile-card pinned-post-cont'><h4>" + window.sL.pinned + "</h4>" + html + "</div>";
		return divHtml;
	}

function setUpDropDown(){

$('body').off('click','.sm-ellipsis');
$('body').on('click','.sm-ellipsis',function(e){

	if(!$(e.target).is('.sm-ellipsis') && $(e.target).closest('.sm-ellipsis').length>0) {
		// don't show/hide when clicking inside the menu
		}

	// hide
	else if($(this).hasClass('dropped')) {
		$(this).removeClass('dropped');
		$(this).children('.dropdown-menu').remove();
		}

	// show
	else {
		$(this).addClass('dropped');

		var closestStreamItem = $(this).closest('.queet').parent('.stream-item');
		var streamItemUsername = closestStreamItem.attr('data-user-screen-name');
		var streamItemUserID = closestStreamItem.attr('data-user-id');
		var streamItemID = closestStreamItem.attr('data-quitter-id');
		var streamItemUserSandboxed = closestStreamItem.hasClass('sandboxed');
		var streamItemUserSilenced = closestStreamItem.hasClass('silenced');

		// menu
		var menuArray = [];

		// pinn the notice
		if(streamItemUserID == window.loggedIn.id && $(e.target).parent().parent().parent().parent().parent().parent().parent().parent('.pinned-post-cont').length != 1){
				menuArray.push({
				type: 'function',
				functionName: 'makePinned',
				functionArguments: {
					streamItemID: streamItemID
					},
				label: window.sL.pinVerb
				});
			}

		// pinOut the notice
		if(streamItemUserID == window.loggedIn.id && $(e.target).parent().parent().parent().parent().parent().parent().parent().parent('.pinned-post-cont').length == 1){
				menuArray.push({
				type: 'function',
				functionName: 'makePinnedOut',
				functionArguments: {
					streamItemID: streamItemID
					},
				label: window.sL.pinOutVerb
				});
			}

		// delete my notice, or others notices for mods with rights
		if(streamItemUserID == window.loggedIn.id || window.loggedIn.rights.delete_others_notice === true) {
			menuArray.push({
				type: 'function',
				functionName: 'deleteQueet',
				functionArguments: {
					streamItemID: streamItemID
					},
				label: window.sL.deleteVerb
				});
			}
		
		// block/unblock if it's not me
		if(streamItemUserID != window.loggedIn.id) {
			if(userIsBlocked(streamItemUserID)) {
				menuArray.push({
					type: 'function',
					functionName: 'unblockUser',
					functionArguments: {
						userId: streamItemUserID
						},
					label: window.sL.unblockUser.replace('{username}',streamItemUsername)
					});
				}
			else {
				menuArray.push({
					type: 'function',
					functionName: 'blockUser',
					functionArguments: {
						userId: streamItemUserID
						},
					label: window.sL.blockUser.replace('{username}',streamItemUsername)
					});
				}

			// mute profile pref
			menuArray.push({
				type: 'profile-prefs-toggle',
				namespace: 'qvitter',
				topic: 'mute:' + streamItemUserID,
				label: window.sL.muteUser,
				enabledLabel: window.sL.unmuteUser,
				tickDisabled: true,
				callback: 'hideOrShowNoticesAfterMuteOrUnmute'
				});
			}

		// moderator actions
		menuArray = appendModeratorUserActionsToMenuArray(menuArray,streamItemUserID,streamItemUsername,streamItemUserSandboxed,streamItemUserSilenced);

		// add menu to DOM and align it
		var menu = $(getMenu(menuArray)).appendTo(this);
		alignMenuToParent(menu,$(this));
		}
	});

}


function makePinned(postID){
	postMarkDMToAPI("qvitterplus/pinned/set.json" , function(){
		pinnedReplace = true;
		searchForProfile();
		$('body').trigger('click');
		} , "post_id=" + postID.streamItemID);
	//console.log(postID);
}

function makePinnedOut(postID){
	postMarkDMToAPI("qvitterplus/pinned/set.json" , function(){
		$('div:not(".hover-card") .profile-card:not(".pinned-post-cont")').find(".pinned-post-cont").remove();
		} , "post_id=" + postID.streamItemID + "&delete=true");
	//console.log(postID);
}

function addProfileCardToDOMFixed(data) {


	// change design
	changeDesign({backgroundimage:data.userArray.background_image, backgroundcolor:data.userArray.backgroundcolor, linkcolor:data.userArray.linkcolor});

	// remove any old profile card and show profile card
	$('#feed').siblings('.profile-card').remove();
	$('#feed').before(data.profileCardHtml);
	searchForProfile();
	}