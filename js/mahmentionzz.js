﻿//STOP QOUTING MEEEEEE GET OFF MAHMENTIONZZZ

$(document).ready(function(){
document.addEventListener("languageReady" , function(){
			//window.setInterval(insertQuoteButton , 100);
		});
});


window.qps.add("QueetHTML" , "addQuoteButton" , function(){

	//get data
	var obj = arguments[0];
	var tempDom = $.parseHTML(obj.queetHtml);
	var quoteHtml = '<li class="action-quote-container"><a class="with-icn"><span class="icon quote-right" title="' + window.sL.quoteVerb + '"></span></a></li>';
	$(tempDom).find('.action-rt-container').before(quoteHtml);
	
	obj.queetHtml = $(tempDom).prop("outerHTML");


	//return modified data
	return obj;

});


/*--------------------------------------------------
*
*
*		CLICKING QUOTE BUTTON
*
*
------------------------------------------------------*/

$('body').on('click','.action-quote-container .icon',function(){
	var this_stream_item = $(this).closest('.stream-item');
	var this_queet = this_stream_item.children('.queet');

	var getLocation = function(href) {
	    var l = document.createElement("a");
	    l.href = href;
	    return l;
	};
	
	var this_queet_link = this_queet.find('.created-at').find('a').attr('href');
	var this_queet_sender = getLocation(this_queet.find('.account-group').attr('href'));
	var this_queet_handle = "@" + this_queet_sender.pathname.replace("/" , "");
	

	var this_hostname = window.location.hostname.replace("www." , "");
	if( this_hostname != this_queet_sender.hostname.replace("www." , "")){
		this_queet_handle += "@" + this_queet_sender.hostname.replace("www." , "");
	} 

	var quote_insert = "<br><br>" + this_queet_handle + "<br>" + this_queet_link;
	//console.log(quote_insert);

	//poppup modal window
	popUpAction('popup-compose', window.sL.compose,queetBoxPopUpHtml(),false);
	var queetBoxWidth = $('#popup-compose').find('.inline-reply-queetbox').width()-20;
	$('#popup-compose').find('.queet-box-syntax').width(queetBoxWidth);
	$('#popup-compose').find('.syntax-middle').width(queetBoxWidth);
	$('#popup-compose').find('.syntax-two').width(queetBoxWidth);
	$('#popup-compose').find('.queet-box').trigger('click');
	$('#popup-compose').find('.queet-box').html(quote_insert);
});


/*--------------------------------------------------
*
*
*		REPLACE QUEET FUNCTION
*
*
------------------------------------------------------*/


