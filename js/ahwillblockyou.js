﻿//Stahp sending me those DMS or Ah will block YOUUUUU!!!
var checkFordmBoxInterval;// = window.setInterval(function(){ if(window.loggedIn != false) refreshDMBox(); } , 60000);

$(document).ready(function(){
	document.addEventListener("languageReady" , insertDMs);
	buildProfileCard = buildProfileCardFixed;
	buildUserStreamItemHtml = buildUserStreamItemHtmlFixed;
})

function insertDMs(){
	

	if(window.loggedIn != false){
		var dmLabel = 'Direct Messages';
		var back = cancerDefBack;
		if(window.loggedIn.backgroundcolor != false) back = window.loggedIn.backgroundcolor;
		if(window.sL.dmTop != undefined) dmLabel = window.sL.dmTop;
		var insert = "<div id='top-dm' class='hidden' data-tooltip='" + dmLabel + "' style='display: block;' ></div>";
		
		$("#top-compose").after(insert);		
		
		var style = $("#dynamic-styles").find("style").text();

		style = style.replace("#top-compose," , "#top-compose,#top-dm,");

		$("#dynamic-styles").find("style").text(style);

		refreshDMBox();

		$('#top-dm').on("click" , function(){

			makeDMBox();
		
		});
		}
	
}

function makeDMBox(){
	var body = "<div id='popup-dm-container'><div class='spinner'>\
  			<div class='double-bounce1' ></div>\
  			<div class='double-bounce2' ></div>\
		</div>\
		</div>";
	var dmTop = "Direct Messages";
	if(window.sL.dmTop != undefined) dmTop = window.sL.dmTop;
	popUpAction('popup-dm', dmTop,body , "<div></div>",false);


	fillDMBox();

	//testowe
}

/* ·
   ·
   ·   Ask for data for dmBox
   ·
   · · · · · · · · · · · · · */
function fillDMBox(){
	
	if(window.dmBox == undefined) {
		refreshDMBox();
		window.dmBox.waiting = true;
	} else {
		fillConvos();
	}
}


/* ·
   ·
   ·   Make an Ajax query
   ·
   · · · · · · · · · · · · · */
function refreshDMBox(){
	var since_id = 0;

	if(window.dmBox == undefined){
		var cache = localStorageObjectCache_GET('qpdmBox' , window.loggedIn.id);
		//if(cache) window.dmBox = new Object();
		//if(cache) window.dmBox.array = cache;
		if(cache) window.dmBox = JSON.parse(cache);
	}
	if(window.dmBox != undefined){
		if(!verifyDMBox()){
			delete localStorage["qpdmBox-" + loggedIn.id]; 
			window.dmBox = undefined;
		}
	}
	
	if(window.dmBox != undefined ) since_id = window.dmBox.since_id;
	if(window.dmBox == undefined) window.dmBox = new Object();
	window.dmBox.page = 1;
	window.dmBox.since_id = since_id;
	window.dmBox.ajax = getFromAPI("qvitterplus/messages.json?all=true&since_id=" + window.dmBox.since_id + "&page=" + window.dmBox.page , getDMData);
}

/* ·
   ·
   ·   Verify dmBox
   ·
   · · · · · · · · · · · · · */

function verifyDMBox(){
	if(window.dmBox.users !== undefined){
		if(window.dmBox.users.length > 0){
			for(var i = 0; i < window.dmBox.users.length;i++){
				if(window.dmBox.users[i].data != undefined){
					if(window.dmBox.users[i].data.length > 0){
						return true;
					} else return false;
				} else return false;
			}
		} else return false;
	} else return false;
}
   
/* ·
   ·
   ·   Add a conversation
   ·
   · · · · · · · · · · · · · */

function addDMConvo(convoPartner , lastMessage){
	
	var convoBox = $('#popup-dm-container');
	
	if( convoBox != undefined){
	
		var domclass = 'convo-partner';
		if(parseInt(convoPartner.new) > 0) domclass += " new";
		
		var insert = "<div class='" + domclass + "' user-id='" + convoPartner.id + "' screen-name='" + convoPartner.screen_name + "'><a href='" + convoPartner.statusnet_profile_url + "'><div class='avatar' style='background: url(\"" + convoPartner.profile_image_url_profile_size + "\"); background-size: cover;'></div><span class='username'><h4>" + convoPartner.name + "</h4><p class='screen-name'>@" + convoPartner.screen_name + "</p></span></a><p class='last-message'>" + lastMessage + "</p></div>";
		convoBox.append(insert);
		
	}

}

/* ·
   ·
   ·   Callback DM function after messages are downloaded
   ·
   · · · · · · · · · · · · · */

function getDMData(data){

	if(window.dmBox.array == undefined){
		window.dmBox.array = data;
	} else{

		window.dmBox.array = window.dmBox.array.concat(data);

	}
		
	if(window.dmBox.array != undefined) window.dmBox.array.sort(function(a , b){
	
		return a.id - b.id;	
		
	});

	window.dmBox.page++;
	if(data.length > 0){
		window.dmBox.ajax = getFromAPI("qvitterplus/messages.json?all=true&since_id=" + window.dmBox.since_id + "&page=" + window.dmBox.page , getDMData);
	} else {
		
		if(window.dmBox.array.length > 0) window.dmBox.since_id = window.dmBox.array[window.dmBox.array.length - 1].id;
		
		dmSortConvos();
		
		if(checkFordmBoxInterval == undefined) checkFordmBoxInterval=window.setInterval(function(){ refreshDMBox(); } , 10000);
		if($("#popup-dm-convo-container").length == 0){
			window.clearInterval(checkFordmBoxInterval);
			checkFordmBoxInterval=window.setInterval(function(){ refreshDMBox(); } , 60000);
		} else {
			window.clearInterval(checkFordmBoxInterval);
			checkFordmBoxInterval=window.setInterval(function(){ refreshDMBox(); } , 8000);
		
		}
		
	}

}


/* ·
   ·
   ·   Sort Conversations by users
   ·
   · · · · · · · · · · · · · */
function dmSortConvos(){
	if(window.dmBox.array != undefined && window.dmBox.array.length != 0){
		
		var users = [];
		if(window.dmBox.users != undefined) users = window.dmBox.users;
		for(var j = 0; j<users.length;j++) users[j].new = 0;
		var data = window.dmBox.array;
		window.dmBox.array = [];
		var found = false;
		for(var i = 0; i<data.length;i++){
			for(var j = 0; j<users.length;j++){

				if(users[j].id == data[i].sender.id && !userDataSearch(data[i].id , users[j])){
					users[j].data.push(data[i]);
					found = true;
					users[j].profile_image_url_profile_size = data[i].sender.profile_image_url_profile_size;
					if(data[i].read == 0 && data[i].recipient.id == window.loggedIn.id) users[j].new++;
				} else if(users[j].id == data[i].recipient.id && !userDataSearch(data[i].id , users[j])){
					users[j].data.push(data[i]);
					found = true;
					users[j].profile_image_url_profile_size = data[i].recipient.profile_image_url_profile_size;
				}
				
								

				users[j].data.sort(function(a , b){
					
					return a.id - b.id;	
					
				});
			}
			if(!found){
					if(data[i].sender.id != window.loggedIn.id){
						users.push(JSON.parse(JSON.stringify(data[i].sender)));
						
						
					}
					else if(data[i].recipient.id != window.loggedIn.id){
						users.push(JSON.parse(JSON.stringify(data[i].recipient)));					
					}
					users[users.length - 1].data = [data[i]];
					users[users.length - 1].new = 0;
					
					if(data[i].read == 0 && data[i].sender.id != window.loggedIn.id) users[users.length - 1].new++;
					
				}

				found = false;
		}

		window.dmBox.users = users;
		
		
		
	}
	
	if(window.dmBox.users != undefined){
		dmNewIcon();
		fillConvos();
	}
	
	
}

function userDataSearch(id , user){
	var found = false;
	if(user.data.length > 0){
		for(var i = 0; i < user.data;i++){
			if(user.data[i].id == id) found = true;
		}
	} 
	
	return found;
}

/* ·
   ·
   ·   Fill Conversations window with Convos
   ·
   · · · · · · · · · · · · · */

function fillConvos(){
	
	if(window.dmBox.users != undefined && window.dmBox.users.length> 0){
		//fill convos window if visible
		if($('#popup-dm-container') != undefined){ 
			
			$('#popup-dm-container').empty();
			
			for(var i = 0; i< window.dmBox.users.length;i++){
				
				var simple_user = new Object();
				
				simple_user.id = window.dmBox.users[i].id;
				simple_user.name = window.dmBox.users[i].name;
				simple_user.screen_name = window.dmBox.users[i].screen_name;
				simple_user.profile_image_url_profile_size = window.dmBox.users[i].profile_image_url_profile_size;
				simple_user.new = window.dmBox.users[i].new;
				simple_user.statusnet_profile_url = window.dmBox.users[i].statusnet_profile_url;
	
				addDMConvo(simple_user , window.dmBox.users[i].data[window.dmBox.users[i].data.length - 1].text);
	
			}
		}

		//fill a Conversation window if visible
		if($('#popup-dm-convo-container').length > 0){
			fillConvoWindow();
		}
	
	} else {
		var info="<div>No messages</div>";
		$('#popup-dm-container').empty();
		$('#popup-dm-container').append(info);
	}

	
}

/* ·
   ·
   ·   Change the number of new messages on icon
   ·
   · · · · · · · · · · · · · */

function dmNewIcon(){
	var number = 0;
	var string = '';
	$('#top-dm strong').remove();


	for(var i = 0; i < window.dmBox.users.length; i++){
		window.dmBox.users[i].data = dmUnique(window.dmBox.users[i].data);
		number += window.dmBox.users[i].new;
	}

	if(number < 100){
		string = number;
	} else {
		string = "99+";
	}

	/*window.dmBox.new = parseInt(number);	
	
	if(window.notifications || window.dmBox.new){
		document.title = '(' + ( parseInt(window.notifications) + parseInt(window.dmBox.new) ) + ') ' + window.siteTitle;
	} else {
		document.title = window.siteTitle;
	}*/
	

	if(number != 0){
	var insert = "<strong>" + string + "</strong>";
	
	
	
	
	
	$('#top-dm').append(insert);
	}

	window.dmBox.new = number;

	var cache = localStorageObjectCache_STORE('qpdmBox' , window.loggedIn.id , JSON.stringify(window.dmBox));
	localStorageObjectCache_STORE('dmBoxSince' , window.loggedIn.screen_name , window.dmBox.since_id);
}

function dmUnique(array){
	var unique = [];
	var uniqueId = [];
	for(var i = 0; i < array.length; i++){
		if(uniqueId.indexOf(array[i].id) == -1){
			unique.push(array[i]);
			uniqueId.push(array[i].id);
		} 
	}
	return unique;
}

function fillConvoWindow(){
	var user = null;

	//find user in array if exists
	if(window.dmBox != undefined){
		if(window.dmBox.users != undefined && window.dmBox.users.length > 0){
			var users = window.dmBox.users;
			for(var i = 0; i < users.length; i++){
			
				if(window.dmBox.user_id == users[i].id){
					user = users[i];
					
				}
			
			}

		}
	} else {
		window.dmBox = new Object();
	}

	if(window.dmBox != undefined && user == null){
		if(window.dmBox.user_id != undefined && window.dmBox.user_id != null){
			getFromAPI("users/show.json?id=" + window.dmBox.user_id , function(data){
				if(window.dmBox.users == undefined) window.dmBox.users = [];
				data.data = [];
				window.dmBox.users.push(data);
				fillConvoWindow();
			});
		}
		
	} else if(window.dmBox != undefined){
		var readArray = []; //read messages array
		var insert = '';
		for(var j = 0; j < user.data.length; j++){
			var buffer = '';
			var side = '';

			if(user.data[j].recipient.id == window.loggedIn.id){
				side = "left"
				user.data[j].sender.profile_image_url_profile_size = user.profile_image_url_profile_size;
				if(user.data[j].read == 0) readArray.push(user.data[j].id);
				user.data[j].read = 1;
				user.new = 0;		
			} else {
				 side="right";
				user.data[j].sender.profile_image_url_profile_size = loggedIn.profile_image_url_profile_size;
			}

			buffer = "<div class='message-box " + side + "' message-id='" + user.data[j].id + "' user-id='" + user.data[j].sender.id + "' screen-name='" + user.data[j].sender.screen_name  + "'>\
      					<a href='" + user.data[j].sender.statusnet_profile_url + "'><span class='avatar' style='background: url(\"" + user.data[j].sender.profile_image_url_profile_size + "\"); background-size: cover;'></span><span class='screen-name' style='display:none'>" + user.data[j].sender.screen_name + "</span></a><span class='message'>" + user.data[j].text + "</span>\
    				</div>";
	
			
			
			insert += buffer;
		}
		
		var arrayString = ''; //read messages variable
		for(var i = 0; i<readArray.length; i++){
			arrayString += "array[]=" + readArray[i] + "&";
		}

		if(arrayString != ''){
			postMarkDMToAPI('qvitterplus/messages/new.json', function(){
				dmNewIcon(); //Refresh the number of new messages
			} , arrayString);
		}
		var oldNumber = 0;
		var newNumber = 0;
		oldNumber = $("#popup-dm-convo-messages").children().length;
		$("#popup-dm-convo-messages").empty();
		$("#popup-dm-convo-messages").append(insert);
		newNumber = $("#popup-dm-convo-messages").children().length;

		if(oldNumber < newNumber) dmScrollDown();	
					
		

	}

	window.clearInterval(checkFordmBoxInterval);
	checkFordmBoxInterval=window.setInterval(function(){ refreshDMBox(); } , 8000);
	
}

function dmScrollDown(){
	$("#popup-dm-convo-messages").animate({ scrollTop: $("#popup-dm-convo-messages")[0].scrollHeight }, "slow");
  return false;
}

/* ·
   ·
   ·   dmBox Conversation Partner Click
   ·
   · · · · · · · · · · · · · */

$('body').on('click','.convo-partner',function(){
	if(window.dmBox == undefined) window.dmBox = new Object();
	
	window.dmBox.user_id = $(this).attr('user-id');
	window.dmBox.user_screen_name = $(this).attr('screen-name');
	$('.modal-close').click();
	makeConvoWindow();
	
});



/* ·
   ·
   ·   Make convo window
   ·
   · · · · · · · · · · · · · */
function makeConvoWindow(){
	var body = "<div id='popup-dm-convo-container'><span class='back-button topbar'></span><div id='popup-dm-convo-messages'></div>" + queetBoxPopUpHtml() + "</div>";
	var userHandle = '';
	if(window.dmBox != undefined) if(window.dmBox.user_screen_name != undefined) userHandle = window.dmBox.user_screen_name;
	popUpAction('popup-dm', window.sL.dmTop + ": @" + userHandle,body , "<div></div>",false);
	
	
	
	fillConvoWindow();
	var queetBoxWidth = $('#popup-dm').find('.inline-reply-queetbox').width()-20;
	$('#popup-dm').find('.queet-box-syntax').width(queetBoxWidth);
	$('#popup-dm').find('.syntax-middle').width(queetBoxWidth);
	$('#popup-dm').find('.syntax-two').width(queetBoxWidth);
	$('#popup-dm').find('.queet-box').trigger('click');
}

$('body').on('click','#popup-dm-convo-container .back-button',function(){

	$('.modal-close').click();
	makeDMBox()

});


//SEND

$('body').on('click','.user-menu-dm',function(e){
	if(window.dmBox == undefined) window.dmBox = new Object();
	
	window.dmBox.user_id = $(this).attr('data-user-id');
	window.dmBox.user_screen_name = $(this).attr('data-screen-name');

	$.each($('.hover-card'),function(){
			
					$('[data-hover-card-active="' + $(this).data('card-created') + '"]').removeAttr('data-hover-card-active');
					$('#hover-card-caret-' + $(this).data('card-created')).remove();
					$(this).remove();
					
			});

	makeConvoWindow();
	
});

/* ·
   ·
   ·   DM send message
   ·
   · · · · · · · · · · · · · */
$('body').on('click', '#popup-dm-convo-container .queet-button button',function () {
	var queetBox = $(this).parent().parent().siblings('.queet-box');
		var queetBoxID = queetBox.attr('id');

		// jquery's .text() function is not consistent in converting <br>:s to \n:s,
		// so we do this detour to make sure line breaks are preserved
		queetBox.html(queetBox.html().replace(/<br>/g, '{{{lb}}}'));
		var queetText =  $.trim(queetBox.text().replace(/^\s+|\s+$/g, '').replace(/\n/g, ''));
		queetText = queetText.replace(/{{{lb}}}/g, "\n");
		$('#popup-dm-convo-container .queet-box').html('');
		$('#popup-dm-convo-container .queet-box .queet-button button').addClass('disabled');
		postMarkDMToAPI("qvitterplus/messages/new.json" , function(){
			var ok = true;
			if(typeof attributes != 'undefined'){
				if(typeof attributes[0] == 'boolean')	ok=attributes[0];		
			} 
			if(ok){ 
				$('#popup-dm-convo-container .queet-box').html('');
				$('#popup-dm-convo-container .queet-box').attr('data-cached-text' , '');

				localStorageObjectCache_STORE('queetBoxInput',queetBox.attr('id'),false);
				if(window.dmBox.ajax != undefined) window.dmBox.ajax.abort();
				refreshDMBox();
			} else {
				$('#popup-dm-convo-container .queet-box').html($('#popup-dm-convo-container .queet-box').attr('data-cached-text'));			
			}
			$('#popup-dm-convo-container .queet-box .queet-button button').removeClass('disabled');
		}, "user=" + window.dmBox.user_id + "&text=" + queetText); 
		
});

$('body').off('click', '.queet-toolbar button');

$('body').on('click', '.queet-toolbar button',function () {

	if($(this).hasClass('enabled') && $(this).parent().parent().parent().parent().attr('id') != 'popup-dm-convo-container') {

		// set temp post id
		if($('.temp-post').length == 0) {
			var tempPostId = 'stream-item-temp-post-i';
			}
		else {
			var tempPostId = $('.temp-post').attr('id') + 'i';
			}

		var queetBox = $(this).parent().parent().siblings('.queet-box');
		var queetBoxID = queetBox.attr('id');

		// jquery's .text() function is not consistent in converting <br>:s to \n:s,
		// so we do this detour to make sure line breaks are preserved
		queetBox.html(queetBox.html().replace(/<br>/g, '{{{lb}}}'));
		var queetText =  $.trim(queetBox.text().replace(/^\s+|\s+$/g, '').replace(/\n/g, ''));
		queetText = queetText.replace(/{{{lb}}}/g, "\n");

		var queetTempText = replaceHtmlSpecialChars(queetText.replace(/\n/g,'<br>')); // no xss
		queetTempText = queetTempText.replace(/&lt;br&gt;/g,'<br>'); // but preserve line breaks
		var queetHtml = '<div id="' + tempPostId + '" class="stream-item conversation temp-post" style="opacity:1"><div class="queet"><span class="dogear"></span><div class="queet-content"><div class="stream-item-header"><a class="account-group"><img class="avatar" src="' + $('#user-avatar').attr('src') + '" /><strong class="name">' + $('#user-name').html() + '</strong> <span class="screen-name">@' + $('#user-screen-name').html() + '</span></a><small class="created-at"> ' + window.sL.posting + '</small></div><div class="queet-text">' + queetTempText + '</div><div class="stream-item-footer"><ul class="queet-actions"><li class="action-reply-container"><a class="with-icn"><span class="icon sm-reply" title="' + window.sL.replyVerb + '"></span></a></li><li class="action-del-container"><a class="with-icn"><span class="icon sm-trash" title="' + window.sL.deleteVerb + '"></span></a></li></i></li><li class="action-fav-container"><a class="with-icn"><span class="icon sm-fav" title="' + window.sL.favoriteVerb + '"></span></a></li></ul></div></div></div></div>';
		queetHtml = detectRTL(queetHtml);

		// popup reply
		if($('.modal-container').find('.toolbar-reply button').length>0){
			var in_reply_to_status_id = $('.modal-container').attr('id').substring(12);
			}
		// if this is a inline reply
		else if(queetBox.parent().hasClass('inline-reply-queetbox')) {
			var in_reply_to_status_id = queetBox.closest('.stream-item').attr('data-quitter-id');
			}
		// not a reply
		else {
			var in_reply_to_status_id = false;
			}

		// remove any popups
		$('.modal-container').remove();

		// try to find a queet to add the temp queet to:
		var tempQueetInsertedInConversation = false;

		// if the queet is in conversation, add it to parent's conversation
		if($('.stream-item.replying-to').length > 0 && $('.stream-item.replying-to').hasClass('conversation')) {
			var insertedTempQueet = $(queetHtml).appendTo($('.stream-item.replying-to').parent());
			findAndMarkLastVisibleInConversation($('.stream-item.replying-to').parent());
			insertedTempQueet.parent().children('.view-more-container-bottom').remove(); // remove any view-more-container-bottom:s, they only cause trouble at this point
			tempQueetInsertedInConversation = true;
			}
		// if the queet is expanded, add it to its conversation
		else if($('.stream-item.replying-to').length > 0 && $('.stream-item.replying-to').hasClass('expanded')) {
			var insertedTempQueet = $(queetHtml).appendTo($('.stream-item.replying-to'));
			findAndMarkLastVisibleInConversation($('.stream-item.replying-to'));
			insertedTempQueet.parent().children('.view-more-container-bottom').remove(); // remove any view-more-container-bottom:s, they only cause trouble at this point
			tempQueetInsertedInConversation = true;
			}
		// maybe the replying-to class is missing but we still have a suiting place to add it
		else if($('.stream-item.expanded[data-quitter-id="' + in_reply_to_status_id + '"]').length > 0) {
			var insertedTempQueet = $(queetHtml).appendTo($('.stream-item.expanded[data-quitter-id="' + in_reply_to_status_id + '"]'));
			findAndMarkLastVisibleInConversation($('.stream-item.expanded[data-quitter-id="' + in_reply_to_status_id + '"]'));
			insertedTempQueet.parent().children('.view-more-container-bottom').remove(); // remove any view-more-container-bottom:s, they only cause trouble at this point
			tempQueetInsertedInConversation = true;
			}
		// if we can't find a proper place, add it to top and remove conversation class
		// if this is either 1) our home/all feed, 2) our user timeline or 3) whole site or 4) whole network
		else if(window.currentStreamObject.name == 'friends timeline'
			 || window.currentStreamObject.name == 'my profile'
			 || window.currentStreamObject.name == 'public timeline'
			 || window.currentStreamObject.name == 'public and external timeline') {
			var insertedTempQueet = $(queetHtml).prependTo('#feed-body');
			insertedTempQueet.removeClass('conversation');
			}
		// don't add it to the current stream, open a popup instead, without conversation class
		else {
			popUpAction('popup-sending','','',false);
			var insertedTempQueet = $(queetHtml).prependTo($('#popup-sending').find('.modal-body'));
			insertedTempQueet.removeClass('conversation');
			}

		// maybe post queet in groups
		var postToGroups = '';
		var postToGropsArray = new Array();
		$.each(queetBox.siblings('.post-to-group'),function(){
			postToGropsArray.push($(this).data('group-id'));
			});
		if(postToGropsArray.length > 0) {
			postToGroups = postToGropsArray.join(':');
			}

		// remove any post-to-group-divs
		queetBox.siblings('.post-to-group').remove();

		// remove any replying-to classes
		$('.stream-item').removeClass('replying-to');

		// null reply box
		collapseQueetBox(queetBox);

		// check for new queets (one second from) NOW
		setTimeout('checkForNewQueets()', 1000);

		// post queet
		postQueetToAPI(queetText, in_reply_to_status_id, postToGroups, function(data){ if(data) {

			var queetHtml = buildQueetHtml(data, data.id, 'visible posted-from-form', false, tempQueetInsertedInConversation);

			// while we were waiting for our posted queet to arrive here, it may have already
			// arrived in the automatic update of the feed, so if it's already there, we
			// replace it (but not if the temp queet is inserted in a conversation of course, or if
			// the user has had time to expand it)
			var alredyArrived = $('#feed-body > .stream-item[data-quitter-id-in-stream=' + data.id + ']');
			if(alredyArrived.length > 0 && tempQueetInsertedInConversation === false) {
				if(!alredyArrived.hasClass('expanded')) {
					alredyArrived.replaceWith(queetHtml);
					}
				}
			else {
				var newInsertedQueet = $(queetHtml).insertBefore(insertedTempQueet);
				findAndMarkLastVisibleInConversation(insertedTempQueet.parent());

				// make ranting easier, move the reply-form to this newly created notice
				// if we have not started writing in it, or if it's missing
				// only if this is an expanded conversation
				// and only if we're ranting, i.e. no replies the queetbox
				var parentQueetBox = insertedTempQueet.parent().find('.inline-reply-queetbox');
				if(parentQueetBox.length == 0
				|| parentQueetBox.children('.syntax-middle').css('display') == 'none') {
					if(insertedTempQueet.parent().hasClass('expanded') || insertedTempQueet.parent().hasClass('conversation')) {
						if(parentQueetBox.children('.queet-box').attr('data-replies-text') == '') {
							insertedTempQueet.parent().find('.inline-reply-queetbox').remove();
							newInsertedQueet.children('.queet').append(replyFormHtml(newInsertedQueet,newInsertedQueet.attr('data-quitter-id')));
							}
						}
					}
				}

			// remove temp queet
			insertedTempQueet.remove();

			// clear queetbox input cache
			localStorageObjectCache_STORE('queetBoxInput',queetBox.attr('id'),false);

			// queet count
			$('#user-queets strong').html(parseInt($('#user-queets strong').html(),10)+1);

			// fadeout any posting-popups
			setTimeout(function(){
				$('#popup-sending').fadeOut(1000, function(){
					$('#popup-sending').remove();
					});
				},100);

			}});
		}
	});


//QveetBox collapse fix
$('body').off('blur' , '.queet-box-syntax')
$('body').on('blur','.queet-box-syntax',function (e){
	// empty the mention suggestions on blur, timeout because we want to capture clicks in .mentions-suggestions
	setTimeout(function(){
		$(this).siblings('.mentions-suggestions').empty();
		},10);

	// don't collapse if a toolbar button has been clicked
	var clickedToolbarButtons = $(this).siblings('.queet-toolbar').find('button.clicked');
	if(clickedToolbarButtons.length>0) {
		clickedToolbarButtons.removeClass('clicked');
		return true;
		}

	// don't collapse if an error message discard button has been clicked
	if($(this).siblings('.error-message').children('.discard-error-message').length>0) {
		return true;
		}

	// don't collapse if we're clicking around inside queet-box
	var syntaxTwoBox = $(this).siblings('.syntax-two');
	if(syntaxTwoBox.hasClass('clicked')) {
		syntaxTwoBox.removeClass('clicked');
		return true;
		}

	// don't collapse if we're in a modal
	if($(this).parent().parent().hasClass('modal-body')) {
		return true;
		}

	// don't collapse if we're in a modal
	if($(this).parent().parent().parent().hasClass('modal-body')) {
		return true;
		}

	// collapse if nothing is change
	if($(this).attr('data-replies-text') != 'undefined') {
		var $startText = $('<div/>').append(decodeURIComponent($(this).attr('data-replies-text')));
		if($.trim($startText.text()) == $.trim($(this).text()) || $(this).html().length == 0 || $(this).html() == '<br>'  || $(this).html() == '<br />' || $(this).html() == '&nbsp;' || $(this).html() == '&nbsp;<br>') {
			collapseQueetBox($(this));
			}
		}

	// collapse if empty
	else if($(this).html().length == 0 || $(this).html() == '<br>'  || $(this).html() == '<br />' || $(this).html() == '&nbsp;' || $(this).html() == '&nbsp;<br>') {
		collapseQueetBox($(this));
		}
});



function buildProfileCardFixed(data) {
	
	data = cleanUpUserObject(data);
	
	// use avatar if no cover photo
	var coverPhotoHtml = '';
	if(data.cover_photo !== false) {
		coverPhotoHtml = 'background-image:url(\'' + data.cover_photo + '\')';
		}

	// follows me?
	var follows_you = '';
	if(data.follows_you === true  && window.loggedIn.id != data.id) {
		var follows_you = '<span class="follows-you">' + window.sL.followsYou + '</span>';
		}

	// me?
	var is_me = '';
	if(window.loggedIn !== false && window.loggedIn.id == data.id) {
		var is_me = ' is-me';
		}

	// logged in?
	var logged_in = '';
	if(window.loggedIn !== false) {
		var logged_in = ' logged-in';
		}

	// silenced?
	var is_silenced = '';
	if(data.is_silenced === true) {
		is_silenced = ' silenced';
		}
	// sandboxed?
	var is_sandboxed = '';
	if(data.is_sandboxed === true) {
		is_sandboxed = ' sandboxed';
		}
	// muted?
	var is_muted = '';
	if(isUserMuted(data.id)) {
		is_muted = ' user-muted';
		}

	var followButton = '';

	// follow from external instance if logged out and the user is local
	if(window.loggedIn === false && data.is_local == true) {
		followButton = '<div class="user-actions"><button type="button" class="external-follow-button"><span class="button-text follow-text"><i class="follow"></i>' + window.sL.userExternalFollow + '</span></button></div>';
		}

	// edit profile button if it's me
	else if(window.loggedIn !== false && window.loggedIn.id == data.id) {
		followButton = '<div class="user-actions"><button type="button" class="edit-profile-button"><span class="button-text edit-profile-text">' + window.sL.editMyProfile + '</span></button></div>';
		}

	// follow button for logged in users
	else if(window.loggedIn !== false) {
		followButton = buildFollowBlockbutton(data);
		}
	//dmButton if its someone else ;)
	var dmButton = '';
	if(window.loggedIn !== false && window.loggedIn.id != data.id && data.is_local == true && data.follows_you && data.following){
		dmButton = '<div class="user-menu-dm' + is_silenced + is_sandboxed + logged_in + '" data-tooltip="' + window.sL.dmTalk + '" data-user-id="' + data.id + '" data-screen-name="' + data.screen_name + '"></div>';
		}
	
	// is webpage empty?
	var emptyWebpage = '';
	if(data.url.length<1) {
		emptyWebpage = ' empty';
		}

	// full card html
	var profileCardHtml = '\
		<div class="profile-card' + is_me + logged_in + is_muted + '">\
			<script class="profile-json" type="application/json">' + JSON.stringify(data) + '</script>\
			<div class="profile-header-inner' + is_silenced + is_sandboxed + '" style="' + coverPhotoHtml + '" data-user-id="' + data.id + '" data-screen-name="' + data.screen_name + '">\
				<div class="profile-header-inner-overlay"></div>\
				<a class="profile-picture" href="' + data.profile_image_url_original + '">\
					<img class="avatar profile-size" src="' + data.profile_image_url_profile_size + '" data-user-id="' + data.id + '" />\
				</a>\
				<div class="profile-card-inner">\
					<h1 class="fullname" data-user-id="' + data.id + '">' + data.name + '</h1>\
					<span class="silenced-flag" data-tooltip="' + window.sL.silencedStreamDescription + '">' + window.sL.silenced + '</span>\
					<span class="sandboxed-flag" data-tooltip="' + window.sL.sandboxedStreamDescription + '">' + window.sL.sandboxed + '</span>\
					<h2 class="username">\
						<span class="screen-name" data-user-id="' + data.id + '">@' + data.screen_name + '</span>\
						' + follows_you + '\
					</h2>\
					<div class="bio-container"><p>' + data.description + '</p></div>\
					<p class="location-and-url">\
						<span class="location">' + data.location + '</span>\
						<span class="url' + emptyWebpage + '">\
							<span class="divider"> · </span>\
							<a href="' + data.url + '">' + data.url.replace('http://','').replace('https://','') + '</a>\
						</span>\
					</p>\
				</div>\
			</div>\
			<div class="profile-banner-footer">\
				<ul class="stats">\
					<li class="tweet-num"><a href="' + data.statusnet_profile_url + '" class="tweet-stats">' + window.sL.notices + '<strong>' + data.statuses_count + '</strong></a></li>\
					<li class="following-num"><a href="' + data.statusnet_profile_url + '/subscriptions" class="following-stats">' + window.sL.following + '<strong>' + data.friends_count + '</strong></a></li>\
					<li class="follower-num"><a href="' + data.statusnet_profile_url + '/subscribers" class="follower-stats">' + window.sL.followers + '<strong>' + data.followers_count + '</strong></a></li>\
					<li class="groups-num"><a href="' + data.statusnet_profile_url + '/groups" class="groups-stats">' + window.sL.groups + '<strong>' + data.groups_count + '</strong></a></li>\
				</ul>\
				' + followButton + '\
				<div class="user-menu-cog' + is_silenced + is_sandboxed + logged_in + '" data-tooltip="' + window.sL.userOptions + '" data-user-id="' + data.id + '" data-screen-name="' + data.screen_name + '"></div>\
				' + dmButton + '\
				<div class="clearfix"></div>\
			</div>\
		</div>\
		';
	
	return { userArray: data, profileCardHtml: profileCardHtml };
	}


function buildUserStreamItemHtmlFixed(obj) {

	obj.description = obj.description || '';

	// external
	var ostatusHtml = '';
	if(obj.is_local === false) {
		ostatusHtml = '<a target="_blank" title="' + window.sL.goToTheUsersRemoteProfile + '" class="ostatus-link" href="' + obj.statusnet_profile_url + '"></a>';
		}

	// rtl or not
	var rtlOrNot = '';
	if($('body').hasClass('rtl')) {
		rtlOrNot = 'rtl';
		}

	// following?
	var followingClass = '';
	if(obj.following) {
		followingClass = ' following';
		}

	// blocking?
	var blockingClass = '';
	if(obj.statusnet_blocking) {
		blockingClass = ' blocking';
		}

	// silenced?
	var silencedClass = '';
	if(obj.is_silenced === true) {
		silencedClass = ' silenced';
		}
	// sandboxed?
	var sandboxedClass = '';
	if(obj.is_sandboxed === true) {
		sandboxedClass = ' sandboxed';
		}
	// logged in?
	var loggedInClass = '';
	if(window.loggedIn !== false) {
		loggedInClass = ' logged-in';
		}
	// muted?
	var mutedClass = '';
	if(isUserMuted(obj.id)) {
		mutedClass = ' user-muted';
		}

	var followButton = '';
	if(typeof window.loggedIn.screen_name != 'undefined'  	// if logged in
	   && window.loggedIn.id != obj.id) {	// not if this is me
		if(!(obj.statusnet_profile_url.indexOf('/twitter.com/')>-1 && obj.following === false)) { // only unfollow twitter users
			var followButton = buildFollowBlockbutton(obj);
			}
		}

	var dmButton = '';
	if(typeof window.loggedIn.screen_name != 'undefined'  	// if logged in
	   && window.loggedIn.id != obj.id && obj.follows_you && obj.following) {	// not if this is me
			dmButton = '<div class="user-menu-dm ' + silencedClass + sandboxedClass + loggedInClass + '" data-tooltip="' + window.sL.dmTalk + '" data-user-id="' + obj.id + '" data-screen-name="' + obj.screen_name + '"></div>';	
			
		
}
	
	return '<div id="stream-item-' + obj.id + '" class="stream-item user' + silencedClass + sandboxedClass + mutedClass + '" data-user-id="' + obj.id + '">\
				<div class="queet ' + rtlOrNot + '">\
					' + followButton + '\
					<div class="user-menu-cog' + silencedClass + sandboxedClass + loggedInClass + '" data-tooltip="' + window.sL.userOptions + '" data-user-id="' + obj.id + '" data-screen-name="' + obj.screen_name + '"></div>\
					' + dmButton + '\
					<div class="queet-content">\
						<div class="stream-item-header">\
							<a class="account-group" href="' + obj.statusnet_profile_url + '" data-user-id="' + obj.id + '">\
								<img class="avatar profile-size" src="' + obj.profile_image_url_profile_size + '" data-user-id="' + obj.id + '" />\
								<strong class="name" data-user-id="' + obj.id + '">' + obj.name + '</strong>\
								<span class="silenced-flag" data-tooltip="' + window.sL.silencedStreamDescription + '">' + window.sL.silenced + '</span> \
								<span class="sandboxed-flag" data-tooltip="' + window.sL.sandboxedStreamDescription + '">' + window.sL.sandboxed + '</span> \
								<span class="screen-name" data-user-id="' + obj.id + '">@' + obj.screen_name + '</span>\
							</a>\
							' + ostatusHtml + '\
						</div>\
						<div class="queet-text">' + obj.description + '</div>\
					</div>\
				</div>\
			</div>';
	}


