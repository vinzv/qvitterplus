<?php


if (!defined('STATUSNET')) {
    // This check helps protect against security problems;
    // your code file can't be executed directly from the web.
    exit(1);
}

class QvitterPlusPlugin extends Plugin
{
	public $schemafix = null;
	public $quotes = null;
	public $trending = null;
	public $directm = null;
	public $custommenu = null;
	public $pinned = null;
	public $emojis = null;
	public $all = false;
	public $spamtags = false;

	static function settings($setting){
		//Module Choice
		$settings["schemafix"] = false;
		$settings["quotes"] = true;
		$settings["trending"] = true;
		$settings["directm"] = true;
		$settings["pinned"] = true;
		$settings["emojis"] = true;
		$settings["spamtags"] = true;
		
		//Custom Menu------------------------------------------
		//First link
		$settings["custommenu"][0]["label"] = "Link 1";
		$settings["custommenu"][0]["href"] = "http://sealion.club";
		$settings["custommenu"][0]["title"] = "Another instance n sheit";
		
		//Second link
		
		$settings["custommenu"][1]["label"] = "Link 2";
		$settings["custommenu"][1]["href"] = "http://jabb.in";
		$settings["custommenu"][1]["title"] = "Another instance n sheit 222";
		
		//Custom Menu Stop --------------------------------------
		
		
		// Trending Setup
		$settings["trending-interval"] = 60000; 	//Refresh interval
		$settings["trending-local"] = true;		//Limit trending calculation to local notices
		$settings["trending-sandboxed"] = false;	//Count tags from sandboxed users
		$settings["trending-hours"] = 24; 		//Timespan from which the trending table is build
		$settings["trending-spam"] = false;		//Filter out spammed tags

		$configphpsettings = common_config('site','qvitterplus') ?: array();
		foreach($configphpsettings as $configphpsetting=>$value) {
			$settings[$configphpsetting] = $value;
		}

		if(isset($settings[$setting])) {
			return $settings[$setting];
		}
		else {
			return false;
		}
	}

	function initialize(){
		
		if(static::settings('schemafix') == true) $this->onCheckSchema();
		return true;
		
	}
	

	function cleanup(){
		return true;
	}

	 public function onCheckSchema()
	 {
	        $schema = Schema::get();
	        $schema->ensureTable('message_info', Message_info::schemaDef());
		$schema->ensureTable('notice_pinned', Notice_pinned::schemaDef());
	        return true;
	 }
	 
	    

	/*function onAutoload($cls)
	{
	    $dir = dirname(__FILE__);
	
	    switch ($cls)
	    {
	    case 'ApiTrendsAction':
	        include_once $dir . '/actions/' . strtolower($cls) . '.php';
		
	        return false;
	    case 'User_greeting_count':
	        include_once $dir . '/'.$cls.'.php';
	        return false;
	    default:
	        return true;
	    }
	} */

	function onRouterInitialized($m)
	{
	    $m->connect('api/qvitterplus/trending.json',
	                array('action' => 'ApiTrending'));
	    
	    // user_id set through GET
	    $m->connect('api/qvitterplus/pinned/get.json',
	                array('action' => 'ApiPinnedGet'));

	    // requires post_id sent through POST
	    $m->connect('api/qvitterplus/pinned/set.json',
	                array('action' => 'ApiPinnedSet'));

            $m->connect('api/qvitterplus/messages.:format',
                    array('action' => 'ApiQPlusDirectMessage',
                          'format' => '(xml|json|rss|atom)'));

	    $m->connect('api/qvitterplus/messages/new.:format',
                    array('action' => 'ApiQPlusDirectMessageNew',
                          'format' => '(xml|json|rss|atom)'));

	    $m->connect('api/qvitterplus/emojis.json',
	                array('action' => 'ApiGetEmojiList'));
						  
		$m->connect('panel/qvitterplus',
                    array('action' => 'qvitterplusadminsettings'));

	    //$m->connect('plugins/Qvitter/img/spritebee.png' , 'img/sprite-quitter.png');

	    return true;
	}

	function onPluginVersion(array &$versions)
	    {
	        $versions[] = array('name' => 'QvitterPlus',
	                            'version' => '0.1',
	                            'author' => 'Jozef Tomaszewski',
	                            'rawdescription' =>
	                          // TRANS: Plugin description.
	                            _m('Some shitzz.'));
	        return true;
	    }

	function onQvitterEndShowScripts($input){
		$dir = Plugin::staticPath('QvitterPlus', '');
		
		if(static::settings("custommenu") != false && count(static::settings("custommenu")) > 0){
		?>
			<script>
				//Custom menu
				var menuInnerHtml = "";
				<?php
					$buffer = "";
					$custommenu = static::settings("custommenu");
					foreach($custommenu as $button){
						$buffer .= "<a href='".$button['href']."' title='".$button['title']."'>".$button['label']."</a>";
					}
					print "var menuInnerHtml=\"".$buffer."\";";
				?>
				
				window.qpSettings = {};
				//Trending settings
				window.qpSettings.trendingInterval = <?php print static::settings("trending-interval") ?>;
			</script>
		<?php
			print "<script src='".$dir."js/tumblrgenders.js?changed=".date('YmdHis',filemtime(__DIR__."/js/tumblrgenders.js"))."'></script>"; //trending
		}


		
		print "<script src='".$dir.'js/cancer.js'."'></script>"; //misc functions
		print "<script src='".$dir.'js/buildthewall.js'."'></script>"; //escape?


			if(static::settings('quotes') != false) print "<script src='".$dir."js/mahmentionzz.js?changed=".date('YmdHis',filemtime(__DIR__."/js/mahmentionzz.js"))."'></script>"; //Quotes

			if(static::settings('trending') != false) print "<script src='".$dir."js/trucklivesmatter.js?changed=".date('YmdHis',filemtime(__DIR__."/js/trucklivesmatter.js"))."'></script>"; //trending

			if(static::settings('directm') != false) print "<script src='".$dir."js/ahwillblockyou.js?changed=".date('YmdHis',filemtime(__DIR__."/js/ahwillblockyou.js"))."'></script>"; //DMs
		
			if(static::settings('pinned') != false) print "<script src='".$dir."js/mesnowflake.js?changed=".date('YmdHis',filemtime(__DIR__."/js/mesnowflake.js"))."'></script>"; //Pinned Posts
		
			if(static::settings('emojis') != false) print "<script src='".$dir."js/XD.js?changed=".date('YmdHis',filemtime(__DIR__."/js/XD.js"))."'></script>"; //Emojis
	}

	function onQvitterEndShowHeadElements(){
		
		$dir = Plugin::staticPath('QvitterPlus', '');
		print "<link href='".$dir."css/qvitterplus.css?changed=".date('YmdHis',filemtime(__DIR__."/css/qvitterplus.css"))."' rel='stylesheet' type='text/css'>";
		
		print "<link href='".$dir."extlib/emojify/css/basic/emojify.css?changed=".date('YmdHis',filemtime(__DIR__."/extlib/emojify/css/basic/emojify.css"))."' rel='stylesheet' type='text/css'>";
		if(static::settings('emojis') != false){
		print "<script src='".$dir."extlib/emojify/emojify.js?changed=".date('YmdHis',filemtime(__DIR__."/extlib/emojify/emojify.js"))."'></script>";
		}
		if(static::settings('directm') != false) print "<link href='".$dir."css/qvitterplusdm.css?changed=".date('YmdHis',filemtime(__DIR__."/css/qvitterplusdm.css"))."' rel='stylesheet' type='text/css'>";
		print "<script src='".$dir."js/triggered.js?changed=".date('YmdHis',filemtime(__DIR__."/js/triggered.js"))."'></script>";
	}

}
?>
