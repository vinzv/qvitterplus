<?php
/**
 * StatusNet, the distributed open-source microblogging tool
 *
 * List of replies
 *
 * PHP version 5
 *
 * LICENCE: This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  Search
 * @package   StatusNet
 * @author    Zach Copley <zach@status.net>
 * @copyright 2008-2010 StatusNet, Inc.
 * @license   http://www.fsf.org/licensing/licenses/agpl-3.0.html GNU Affero General Public License version 3.0
 * @link      http://status.net/
 */

if (!defined('GNUSOCIAL')) { exit(1); }



/**
 *  Returns the top ten queries that are currently trending
 *
 * @category Search
 * @package  StatusNet
 * @author   Zach Copley <zach@status.net>
 * @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html GNU Affero General Public License version 3.0
 * @link     http://status.net/
 *
 * @see      ApiAction
 */
class ApiTrendingAction extends ApiAuthAction
{
    var $callback;
    var $limit;
    var $localTest;
    var $timespan;
    var $spamtags;

    /**
     * Initialization.
     *
     * @param array $args Web and URL arguments
     *
     * @return boolean false if user doesn't exist
     */
    function prepare($args)
    {
        parent::prepare($args);
	$this->limit = $this->arg('limit');
        return true;
    }

    /**
     * Handle a request
     *
     * @param array $args Arguments from $_REQUEST
     *
     * @return void
     */
    function handle($args)
    {
        parent::handle($args);
        $this->showTrends();
    }

    /**
     * Output the trends
     *
     * @return void
     */
    function showTrends()
    {
        // TRANS: Server error for unfinished API method showTrends.
       // $this->serverError(_('API method under construction.'), 501);
		//error_reporting(0);
		$this->localTest = "";
		$this->timespan = "24";
		if(QvitterPlusPlugin::settings("trending-local")) $this->localTest = " AND notice.is_local <> 0";
		if(QvitterPlusPlugin::settings("trending-hours") != false) $this->timespan = QvitterPlusPlugin::settings("trending-hours");
		if(QvitterPlusPlugin::settings("trending-spamtags") != false) $this->spamtags = QvitterPlusPlugin::settings("trending-spamtags");
		$tags = @$this->getTrends($this->limit);
	
		$this->initDocument('json');
		print json_encode($tags);
		$this->endDocument('json');
    }

    function getTrends($limit = 10){

	try{
	$nt = new Notice_tag();

	$nt->selectAs();

	$n = new Notice();

	//$n->selectAs();	

	$pr = new Profile_role();

	
	$n->joinAdd(array('profile_id' , $pr , 'profile_id') , "LEFT");
	
	
	$nt->joinAdd(array('notice_id' , $n , 'id') , "LEFT");
	
	//$nt->joinAdd(array('notice.profile_id' , $pr , 'profile_id'));
	//$n->joinAdd(array('profile_id' , $pr , 'profile_id'));
	
	//$nt->selectAs($n , 'notices_%s');
	
	//$nt->selectAdd('DISTINCT'.$nt->selectAdd());
	$nt->selectAdd('id');
	$nt->selectAdd('tag');
	
	
	$nt->selectAdd('COUNT(DISTINCT id) as number');
	//$nt->groupBy('id');
	$nt->groupBy('tag');
	$nt->orderBy('number DESC');
	
	$nt->whereAdd("TIMESTAMP(notice.created) BETWEEN DATE_SUB(NOW() , INTERVAL ".$this->timespan." HOUR) AND NOW()");
	$nt->whereAdd("(notice.object_type LIKE '%note' OR notice.object_type LIKE '%comment')".$this->localTest);
	
	if(QvitterPlusPlugin::settings("trending-sandboxed") != true){
		$nt->whereAdd("profile_role.role NOT LIKE 'sandboxed' AND profile_role.role NOT LIKE 'silenced' OR profile_role.role IS NULL");
//		$nt->whereAdd("profile_role.role NOT LIKE 'sandboxed' AND profile_role.role NOT LIKE 'silenced' OR profile_role.role IS NULL AND notice_tag.tag NOT LIKE 'nsfw'");
	}

	if(QvitterPlusPlugin::settings("trending-spamtags") != true){
                $nt->whereAdd("notice_tag.tag NOT LIKE 'nsfw'");
//                $nt->whereAdd("notice_tag.tag NOT LIKE '.$this->spamtags.'");
        }

	$nt->limit($limit);
	
	
	//$n->joinAdd($nt);
	//$nt->joinAdd($n);

	$tags = array();
	//echo 'sheit';
	if($nt->find()){
		while($nt->fetch()){
		
		$tag = null;
		$tag->tag = $nt->tag;
		$tag->number = $nt->number;
		$tags[]= $tag;
		//$tags[]->number = $nt->number;
		
	
		}
	}
	

	return $tags;
	} catch (Exception $e){
		echo $e->getMessage();
	}
	
    }

    
}



